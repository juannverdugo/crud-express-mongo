const path = require('path');
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const indexRoutes = require('./routes/index')

// initialize
const app = express();

// settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs');

// middlewares
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false}));

// routes
app.use('/', indexRoutes)

app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
});